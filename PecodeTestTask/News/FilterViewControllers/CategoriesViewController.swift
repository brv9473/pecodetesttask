//
//  CategorysViewController.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 13.02.2021.
//

import UIKit

class CategoriesViewController: TableViewController<String> {

    override func viewDidLoad() {
        super.viewDidLoad()

        dataArray = ["Business", "Entertainment", "General", "Health", "Science", "Sports", "Technology"]
    }
}
