//
//  SourcesViewController.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 13.02.2021.
//

import UIKit

class SourcesViewController: TableViewController<SourceModel> {
    
    // MARK: - Properties
    
    private let service = Service()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSources()
    }
    // MARK: - Private Methods
    
    private func getSources() {
        self.service.getSources(completion: .some({ [weak self] news in
            switch news {
            case .success(let model):
                self?.dataArray = model.sources
                
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
                
            case .failure(let error):
                self?.showErrorAlert(with: error)
            }
        }))
    }
}
