//
//  FilterVC.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 13.02.2021.
//

import UIKit

class TableViewController<DataType: ToString>: UITableViewController {
    
    // MARK: - Properties
    
    var completion: ((DataType) -> Void)?
    var dataArray: [DataType]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")

        cell?.textLabel?.text = dataArray?[indexPath.row].stringValue

        return cell ?? UITableViewCell()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let data = dataArray?[indexPath.row] else { return }
        completion?(data)
        dismiss(animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
