//
//  SearchNewsViewController.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import UIKit

class SearchNewsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak private var searchBar: UISearchBar!
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var filterView: FilterView!
    
    // MARK: - Private Properties
    private let service = Service()
    private var articles: [ArticleModel]?
    private let debouncer = Debouncer(timeInterval: 1.5)
    private let refreshControl = UIRefreshControl()
    
    // MARK: - Properties
        
    private var searchString: String?
    private var category: String?
    private var country: String?
    private var sourceId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
        searchBar.delegate = self
        filterView.deleate = self
        
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        let newsCell = UINib(nibName: "NewsInfoTableViewCell", bundle: nil)
        tableView.register(newsCell, forCellReuseIdentifier: "NewsInfoTableViewCell")
        
        setupUI()
    }
    
    // MARK: - Private Methods
    
    private func setupUI() {
        filterView.setCountryButtonTitle(title: "Country")
        filterView.setCategoryButtonTitle(title: "Category")
        filterView.setSourcesButtonTitle(title: "Sources")
        
        tableView.layer.borderWidth = 1
        tableView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    private func fatchNewData() {
        debouncer.renewInterval()

        debouncer.handler = {
            if self.sourceId != nil {
                self.getNewsBySources()
                
            } else {
                self.getNewsByCountryAndCategory()
            }
        }
    }
    
    private func getNewsBySources() {
        guard let text = self.searchBar.text else { return }
        
        self.service.getNewsBySources(text: text,
                                      sourceID: sourceId!,
                                      completion: .some({[weak self] sources in
            switch sources {
            case .success(let model):
                self?.articles = model.articles
                
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
                
            case .failure(let error):
                self?.showErrorAlert(with: error)
            }
            
        }))
    }
    
    private func getNewsByCountryAndCategory() {
        guard let text = self.searchBar.text else { return }
        
        self.service.getNewsByCountryAndCategory(text: text,
                                                 country: self.country,
                                                 category: self.category,
                                                 completion: .some({ [weak self] news in
                                                    switch news {
                                                    case .success(let model):
                                                        self?.articles = model.articles
                                                        
                                                        DispatchQueue.main.async {
                                                            self?.tableView.reloadData()
                                                        }
                                                        
                                                    case .failure(let error):
                                                        self?.showErrorAlert(with: error)
                                                    }
                                                 }))
    }
    
    @objc private func refreshData() {
            fatchNewData()
            tableView.reloadData()
            refreshControl.endRefreshing()
    }
}

extension SearchNewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        if searchBar.text?.count == 0 {
            tableView.setEmptyView(message: "Enter what news you are looking for")
            
        } else if articles?.count == 0 {
            tableView.setEmptyView(message: "Not found")
            
        } else {
            tableView.restore()
        }
        
        return articles?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsInfoTableViewCell",
                                                 for: indexPath) as! NewsInfoTableViewCell
        
        cell.setTitle(articles?[indexPath.row].title ?? "No Info")
        cell.setSource(articles?[indexPath.row].source?.name ?? "No Info")
        cell.setAuthor(articles?[indexPath.row].author ?? "No Info")
        cell.setDescription(articles?[indexPath.row].description ?? "No Info")
        cell.addNewsImage(from: articles?[indexPath.row].urlToImage ?? "???")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        
        if let url = articles?[indexPath.row].url {
            _ = NewsCoordinator(navigationController).navigateToNewsDetalization(url: url)
        }
    }
    
    func tableView(_ tableView: UITableView,
                   willDisplay cell: UITableViewCell,
                   forRowAt indexPath: IndexPath) {
        
        if indexPath.row == articles!.count - 1 {
            print("<<<<<<<<<<")
        }
    }
}

extension SearchNewsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        searchString = searchBar.text
        fatchNewData()
    }
}

extension SearchNewsViewController: ButtonActionDelegate {
    func onButtonCategoryyTap(sender: UIButton) {
        let categoryFilterVC = NewsCoordinator(navigationController).navigateToCategories(animated: true)
        
        categoryFilterVC.completion = { [unowned self] in
            self.filterView.setCategoryButtonTitle(title: $0 )
            self.category = $0
            self.sourceId = nil
            self.filterView.setSourcesButtonTitle(title: "Sources")
            self.fatchNewData()
        }
    }
    
    func onButtonSourcesTap(sender: UIButton) {        
        let sourcesFilterVC = NewsCoordinator(navigationController).navigateToSources(animated: true)

        sourcesFilterVC.completion = { [unowned self] in
            self.filterView.setSourcesButtonTitle(title: $0.name)
            self.sourceId = $0.id
            self.country = nil
            self.category = nil
            self.filterView.setCategoryButtonTitle(title: "Category")
            self.filterView.setCountryButtonTitle(title: "Country")
            self.fatchNewData()
        }
    }
    
    func onButtonCountryTap(sender: UIButton) {
        let countryFilterVC = NewsCoordinator(navigationController).navigateToCountries(animated: true)

        countryFilterVC.completion = { [unowned self] in
            self.filterView.setCountryButtonTitle(title: $0)
            self.country = $0
            self.sourceId = nil
            self.filterView.setSourcesButtonTitle(title: "Sources")
            self.fatchNewData()
            
        }
    }
}
