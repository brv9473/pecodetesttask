//
//  DetalizationNewsViewController.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import UIKit
import WebKit

class DetalizationNewsViewController: UIViewController {


    // MARK: - Private Properties
    
    private var webView: WKWebView!
    var url: URL?

    override func loadView() {
        webView = WKWebView()
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.load(URLRequest(url: url!))
        
        webView.allowsBackForwardNavigationGestures = true
    }
}
