//
//  NewsInfoTableViewCell.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import UIKit
import Alamofire
import AlamofireImage

class NewsInfoTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak private var newsImage: UIImageView!
    @IBOutlet weak private var title: UILabel!
    @IBOutlet weak private var source: UILabel!
    @IBOutlet weak private var author: UILabel!
    @IBOutlet weak private var newsDescription: UILabel!
    @IBOutlet weak var backgroundContetntView: UIView!
    
    // MARK: - Private Properties
    
    private var imageRequest: DataRequest?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundContetntView.layer.cornerRadius = 16
        backgroundContetntView?.layer.borderColor = UIColor.black.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func addNewsImage(from url: String) {
        
       imageRequest = AF.request(url).responseImage { [unowned self] in
            if case .success(let image) = $0.result {
                self.newsImage.image = image
            }
        }
    }
    
    override func prepareForReuse() {
        imageRequest?.cancel()
        newsImage.image = nil
    }
    
    func setTitle(_ newsTitle: String) {
        title.text = newsTitle
    }
    
    func setSource(_ newsSource: String) {
        source.text = newsSource
    }
    
    func setAuthor(_ newsAuthor: String) {
        author.text = newsAuthor
    }
    
    func setDescription(_ description: String) {
        newsDescription.text = description
    }
}
