//
//  FilterView.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 11.02.2021.
//

import UIKit

protocol ButtonActionDelegate {
    func onButtonCountryTap(sender: UIButton)
    func onButtonCategoryyTap(sender: UIButton)
    func onButtonSourcesTap(sender: UIButton)
}

class FilterView: UIView {

// MARK: - IBOutlets
    
    @IBOutlet weak private var countryButton: UIButton!
    @IBOutlet weak private var categoryButton: UIButton!
    @IBOutlet weak private var sourcesButton: UIButton!
    
    var deleate: ButtonActionDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder),
                                                  owner: self,
                                                  options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        setupUI()
    }
    
    private func setupUI() {
        countryButton.setTitleColor(.black, for: .normal)
        categoryButton.setTitleColor(.black, for: .normal)
        sourcesButton.setTitleColor(.black, for: .normal)
    }
    
     func setCountryButtonTitle(title: String) {
        countryButton.setTitle(title, for: .normal)
    }
    
    func setCategoryButtonTitle(title: String) {
        categoryButton.setTitle(title, for: .normal)
    }
    
    func setSourcesButtonTitle(title: String) {
        sourcesButton.setTitle(title, for: .normal)
    }

    @IBAction func onButtonCountryTap(_ sender: Any) {
        deleate?.onButtonCountryTap(sender: sender as! UIButton)
    }
    
    @IBAction func onButtonCategoryTap(_ sender: Any) {
        deleate?.onButtonCategoryyTap(sender: sender as! UIButton)
    }
    
    @IBAction func onButtonSourcesTap(_ sender: Any) {
        deleate?.onButtonSourcesTap(sender: sender as! UIButton)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        countryButton.layer.cornerRadius = 8
        categoryButton.layer.cornerRadius = 8
        sourcesButton.layer.cornerRadius = 8
    }
}
