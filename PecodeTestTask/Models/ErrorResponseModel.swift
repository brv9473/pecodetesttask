//
//  ErrorResponseModel.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import Foundation

struct ErrorResponseModel: Decodable, Error {
    let status: String
    let message: String
    let code: Int
    
    var localizedDescription: String {
        message
    }
}
