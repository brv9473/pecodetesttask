//
//  NewsModel.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import Foundation

struct ArticleModel: Codable {
    let source: SourceModel?
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
}

struct SourceModel: Codable, ToString {
    let id: String?
    let name: String
    let description: String?
    let url: String?
    let category: String?
    let language: String?
    let country: String?
    
    var stringValue: String { name }
}

// MARK: - Reponse

struct NewsResponseModel: Codable {
    let articles: [ArticleModel]
}

struct SourcesResponseModel: Codable {
    let sources: [SourceModel]
}

// MARK: - Helpers

protocol ToString {
    var stringValue: String { get }
}

extension String: ToString {
    var stringValue: String { self }
}
