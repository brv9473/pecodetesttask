//
//  NewsCoordinator.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import UIKit

final class NewsCoordinator: BaseCoordinator {
    
    func navigateToNewsDetalization(animated: Bool = true, url: String) -> DetalizationNewsViewController {
        let detalizationVC = DetalizationNewsViewController.instantiate
        detalizationVC.url = URL(string: url)
        navigation?.present(detalizationVC, animated: animated, completion: nil)
        
        return detalizationVC
    }
    
    func navigateToCountries(animated: Bool = true) -> CountriesViewController {
        let filterVC = CountriesViewController()
        navigation?.present(filterVC, animated: animated, completion: nil)
        
        return filterVC
    }
    
    func navigateToCategories(animated: Bool = true) -> CategoriesViewController {
        let filterVC = CategoriesViewController()
        navigation?.present(filterVC, animated: animated, completion: nil)
        
        return filterVC
    }
    
    func navigateToSources(animated: Bool = true) -> SourcesViewController{
        let filterVC = SourcesViewController()
        navigation?.present(filterVC, animated: animated, completion: nil)
        
        return filterVC
    }
}
