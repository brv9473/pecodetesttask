//
//  Service.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import Foundation
import Alamofire

class Service {
    func getSources(completion: ((Result<SourcesResponseModel, Error>) -> Void)?) {
        let endPoint = "sources"

        
        ApiManager.sendGet(endPoint: endPoint,
                           queryItems: [],
                           headers: nil,
                           completion: completion)
    }
    
    func getNewsByCountryAndCategory(text: String,
                                     country: String?,
                                     category: String?,
                                     page: Int = 0,
                                     completion: ((Result<NewsResponseModel, Error>) -> Void)?) {
        
        let endPoint = "top-headlines"
        let query = [
            URLQueryItem(name: "q", value: text),
            URLQueryItem(name: "country", value: country),
            URLQueryItem(name: "category", value: category),
            URLQueryItem(name: "pageSize", value: "50"),
            URLQueryItem(name: "page", value: "\(page)")
        ]
        
        ApiManager.sendGet(endPoint: endPoint,
                           queryItems: query,
                           headers: nil,
                           completion: completion)
    }
    
    func getNewsBySources(text: String,
                          sourceID: String,
                          page: Int = 0,
                          completion: ((Result<NewsResponseModel, Error>) -> Void)?) {
        
        let endPoint = "top-headlines"
        let query = [
            URLQueryItem(name: "q", value: text),
            URLQueryItem(name: "sources", value: sourceID),
            URLQueryItem(name: "pageSize", value: "50"),
            URLQueryItem(name: "page", value: "\(page)")
        ]
        
        ApiManager.sendGet(endPoint: endPoint,
                           queryItems: query,
                           headers: nil,
                           completion: completion)
    }
}
