//
//  Constants.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import Foundation

final class Constants {
    
    struct Url {
        static let baseUrl = "newsapi.org"
        static let apiKey = "333d80a3ef134438aaf74a0b7b04248a"
    }
}
