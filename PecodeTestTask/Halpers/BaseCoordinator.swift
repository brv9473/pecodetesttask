//
//  BaseCoordinator.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import UIKit

class BaseCoordinator {
    
    let navigation: UINavigationController?

    init(_ navigation: UINavigationController?) {
        self.navigation = navigation
    }
}
