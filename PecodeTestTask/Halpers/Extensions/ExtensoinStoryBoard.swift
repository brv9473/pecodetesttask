//
//  ExtensoinStoryBoard.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import UIKit

enum Storyboard: String, CaseIterable {
    case detalization = "DetalizationNewsViewController"
}

extension Storyboard {
    var viewControllers: [String] {
        switch self {
        case .detalization:
            return [DetalizationNewsViewController.name]
        }
    }
}

extension Storyboard {
    func instantiateInitialController() -> UIViewController {
        guard let vcToInit = UIStoryboard(name: self.rawValue, bundle: nil)
                .instantiateInitialViewController() else {
            fatalError("Storyboard doesn't have initial controller or there's no storyboard with this name")
        }
        return vcToInit
    }
}

extension Storyboard {
    static func value(byViewController viewController: UIViewController.Type) -> Storyboard {
        return value(byViewController: viewController.name)
    }
    
    static func value(byViewController viewControllerName: String) -> Storyboard {
        for storyboard in Storyboard.allCases
            where storyboard.viewControllers.contains(viewControllerName) {
                return storyboard
        }
        
        fatalError("ViewController: \"\(viewControllerName)\" is not defined in Storyboard enum")
    }
}

protocol Instantiable: class {}

extension Instantiable where Self: UIViewController {
    static var instantiate: Self {
        return UIViewController.instantiate(vcName: self.name)
    }
}

extension UIViewController: Instantiable {
    static var name: String {
        return String(describing: self)
    }
    
    static func instantiate<Instantiable: UIViewController>(vcName: String) -> Instantiable {
        let storyboard = UIStoryboard(name: Storyboard.value(byViewController: vcName).rawValue,
                                      bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: vcName)
                as? Instantiable else {
            fatalError("ViewController: \"\(vcName)\" incorrect cast to \(Instantiable.self)")
        }
        
        return viewController
    }
}
