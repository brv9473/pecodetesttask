//
//  AppDelegate.swift
//  PecodeTestTask
//
//  Created by Rostyslav Bodnar on 10.02.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let firstVC = SearchNewsViewController()
        window?.rootViewController = UINavigationController(rootViewController: firstVC)
        window?.makeKeyAndVisible()
        
        return true
    }
}

